import axios from 'axios';
const KEY = 'AIzaSyCWJECCkkLfqkEZs19OVgcbplvuJfZCnF8'

export default axios.create({

    baseURL: 'https://www.googleapis.com/youtube/v3',
    params: {
        part: 'snippet',
        maxResulst : 5,
        key : KEY
    }
})