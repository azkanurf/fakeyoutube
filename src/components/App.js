import React, {Component} from 'react';
import './App.css';
import Searchbar from './Searchbar';
import youtube from '../apis/youtube';



class App extends Component {
  state = {
    videos: [],
    selectedVideo: null
  }

  handleSubmit = async (termFromSearchBar) =>{
    const response = await youtube.get('/search', {
      params: {
        q: termFromSearchBar
      }
    })
    this.setState({
      videos: response.data.items
    })
  }

  render(){
    return (
      <div className="ui container" style={{marginTop: '1em'}}>
        <Searchbar handleFormSubmit={this.handleSubmit}/>
      </div>
    )
  }
}

export default App;
