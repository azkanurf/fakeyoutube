import React, {Component} from 'react';

class Searchbar extends Component{
	state ={
		term: 'Default text'
	};

	handleChange = (event) => {
		this.setState({
			term: event.target.value
		})
	}

	handleSubmit = (event) =>{
		event.preventDefault();
		this.props.handleFormSubmit(this.state.term)
	}



	render(){
		return(
			<div className ='search-bar ui segment'>
				<form className ="ui form" onSubmit={this.handleSubmit} >
					<div className='field'>
						<label htmlFor="video-search">Video Search</label>
						<input type="text" name="video-search" value={this.state.term} onChange={this.handleChange}/> 
					</div>
				</form>
			</div>
		)
	}
}

export default Searchbar;